# -*- coding: utf-8 -*-

from django.contrib.auth import get_user_model, authenticate

from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response

from trapper.apps.accounts import serializers as accounts_serializers
from trapper.apps.common.views_api import PaginatedReadOnlyModelViewSet

User = get_user_model()


class UserViewSet(PaginatedReadOnlyModelViewSet):
    permission_classes = (permissions.IsAuthenticated, )
    queryset = User.objects.all()
    serializer_class = accounts_serializers.UserSerializer


class TestUserLogin(APIView):
    """
    Temporary solution for testing user login data
    """
    def post(self, request, *args, **kwargs):
        email = request.POST['email']
        password = request.POST['password']
        user = authenticate(email=email, password=password)

        if user is not None:
            return Response({
                'username': user.username,
                'email': user.email,
                'error': '0'
            })
        else:
            return Response({
                'username': '',
                'email': '',
                'error': '1'
            })


