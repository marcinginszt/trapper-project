==============
 Installation
==============

TRAPPER is provided as `Docker <https://www.docker.com/>`_ container and can be deployed on both Linux- and Windows-based servers. However, we recommend Linux distribution based on Debian e.g. `Ubuntu Server <http://www.ubuntu.com/server>`_. TRAPPER uses `Docker composer <https://docs.docker.com/compose/>`_, a tool for defining and running multi-container Docker applications. With this technology all basic services needed to run TRAPPER (including database and FTP servers) can be automatically deployed and configured on a dedicated server with a possibility left open to configure particular services manually (e.g. scenario with an external database server).

Installing Docker Community Edition (CE)
++++++++++++++++++++++++++++++++++++++++

Follow the steps from the official Docker documentation:

1) Ubuntu: 
   https://docs.docker.com/install/linux/docker-ce/ubuntu/

2) Windows:
   https://hub.docker.com/editions/community/docker-ce-desktop-windows


Getting the source code of TRAPPER
++++++++++++++++++++++++++++++++++

1) First, clone the source code of TRAPPER to your local repository:
   
   .. code-block:: console
                   
      $ git clone https://gitlab.com/oscf/trapper-project.git
   
2) To get the most up-to-date version of TRAPPER switch to a branch ``development``:

   .. code-block:: console

      $ git checkout -b development

3) Pull the source code of selected branch: 

   .. code-block:: console

      $ git pull origin development

.. note::
   It is much easier to use Git Bash for cloning git repositories on Windows instead
   of using a standard Windows command line: https://git-scm.com/download/win

   
Running TRAPPER in a production mode
++++++++++++++++++++++++++++++++++++

.. note::
   If you need a custom configuration of TRAPPER (e.g. external database, ftp
   and email services) please check the **Configuration** section for details.
   
1) First, in the project's root directory make a copy of ``docker-compose.yml.dist``
   and rename it to ``docker-compose.yml``. This is your custom docker composer
   configuration file which defines all services (including TRAPPER) that will be
   run together in an isolated environment.
   
2) If you are going to use the FTP server which has its own docker container   
   you have to change the ``ftpd`` section in your ``docker-compose.yml`` file
   by updating ``command`` specification with the IP or name of your production server.
   
3) Build and run all docker containers with the following command:
   
   .. code-block:: console
                   
      $ docker-compose up --build postgres rabbitmq nginx ftpd trapper

.. note::
   The process of building all docker containers can take a while!

4) Now your TRAPPER instance should be up!

.. note::
   Be aware that when you update the source code with ``git pull`` and further
   re-build all (or selected) docker containers, already created TRAPPER database
   will not be modified! Only Django database migrations will be applied. 
   
.. note::
   You can choose which containers you want to build and run e.g. when
   you use an external database it is likely that you do not need to build the
   ``postgres`` container. See **Configuration** section for more details.
              
.. warning::
   During the first run it is possible that you get the error message
   indicating the fatal states of celery. If so, stop the containers
   by typing ``Ctrl + c`` and try to re-start them. 

.. note::
   To learn more about Docker technology including the management of Docker images and
   containers please read the official `Docker documentation <https://docs.docker.com/get-started/>`_.  

      
Running TRAPPER in a development mode
+++++++++++++++++++++++++++++++++++++

To run TRAPPER in a development mode use provided special ``docker-compose-dev.yml``
configuration file:
   
.. code-block:: console
                   
   $ docker-compose up -f docker-compose-dev.yml --build trapper

Now trapper should work in a development mode. Type ``localhost:8000`` in
your web browser to check it out.


Creating admin (superuser) account
++++++++++++++++++++++++++++++++++

In a terminal type the following commands to get into a running
TRAPPER container and to create an admin account:

.. code-block:: console

   $ docker exec -it trapper_trapper_1 bash
   $ cd opt/trapper
   $ python manage.py createsuperuser

After providing a username, email and password you should be able to login
to TRAPPER.

.. note::
   Make sure that ``trapper_trapper_1`` is a proper name for your running
   TRAPPER container. You can check it out using this command:
   ``docker container ls``

SSL certificates
++++++++++++++++

Before initiating TRAPPER in docker containers it is possible to place two
SSL certificates named ``cert.perm`` and ``key.perm`` in
``{PROJECT_ROOT}/nginx/ssl`` directory. These certificates will
be used to configure an HTTPS ``nginx`` server within a dedicated container.
When there is no SSL certificates provided they will be generated automatically
using the `openssl <https://www.openssl.org/>`_ toolkit.  

.. note::        
   For example you can get free SSL certificates from 
   `Let's Encrypt <https://letsencrypt.org>`_.

   
Creating FTP accounts for new users
+++++++++++++++++++++++++++++++++++

In a terminal type the following command to get into a running
FTPD container:

.. code-block:: console
                
   $ docker exec -it trapper_ftpd bash

Then to create a new FTP account run the following command replacing ``<username>``
with a real username:

.. code-block:: console

   $ pure-pw useradd <username> -f /etc/pure-ftpd/passwd/pureftpd.passwd -m \
   -u ftpuser -d /opt/trapper/trapper/external_media/<username>
                
.. note::
   Make sure that ``trapper_ftpd`` is a proper name for your running
   FTPD container. You can check it out using this command:
   ``docker container ls``
   
