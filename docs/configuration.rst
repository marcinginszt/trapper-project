===============
 Configuration
===============

You can override many default settings in TRAPPER by creating the file
``settings_local.py`` in the directory containing the original configuration file
``settings.py`` i.e. ``{PROJECT_ROOT}/trapper/trapper-project/trapper``.
Further in this section we provide some examples of how to customize selected
aspects of TRAPPER.
   
Using external database
+++++++++++++++++++++++

To use an external database provide the following data in your ``settings_local.py``:

.. code-block:: python
                
   DATABASES = {
            'default': {
            'ENGINE': 'django.contrib.gis.db.backends.postgis',
            'NAME': 'trapper',
            'USER': 'trapper',
            'PASSWORD': 'trapper',
            'HOST': 'EXTERNAL_HOST',
            'PORT': '8000'
        }
    }

.. note::
   It is likely that using an external database you do not need to build and run the
   ``postgres`` container anymore.

   
Email service
+++++++++++++

The working email service is required to make the notification framework
working correctly. However, you can turn the notification framework off by putting
the following in your ``settings_local.py``:

.. code-block:: python
                
   EMAIL_NOTIFICATIONS = False
   EMAIL_NOTIFICATIONS_RESEARCH_PROJECT = False

To configure the email backend you should set the following attributes
(example with gmail): 

.. code-block:: python
                
   EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
   EMAIL_HOST = 'smtp.gmail.com'
   EMAIL_HOST_PASSWORD = 'xxx'
   EMAIL_HOST_USER = 'xxx@gmail.com'
   EMAIL_PORT = 587

.. warning::
   These settings should not be changed in the *settings.py* file because
   it can be accidentally pushed to the repository. Use the *settings_local.py*
   instead (this file is excluded from git).

.. note::
   If local SMTP server is used it has to be configured to be able to send
   email messages. Configuration of the SMTP server is out of the scope of this
   documentation.

