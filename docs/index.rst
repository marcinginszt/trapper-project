.. trapper-project documentation master file, created by
   sphinx-quickstart on Fri Apr 12 15:56:50 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to trapper-project's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   overview
   installation
   configuration
   technologies
   license
   
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
