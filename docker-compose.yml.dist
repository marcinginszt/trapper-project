version: '3.2'
services:
  postgres:
    build: ./postgres
    environment:
      - POSTGRES_USER=trapper
      - POSTGRES_PASSWORD=trapper
      - POSTGRES_DB=trapper
    volumes:
      - pgdata:/var/lib/postgresql/data
  rabbitmq:
    image: rabbitmq
  trapper:
    build: ./trapper
    container_name: trapper
    depends_on:
      - postgres
      - rabbitmq
    env_file:
      - trapper.env
    tty: true
    volumes:
      - ./trapper/trapper-project/trapper/external_media:/opt/trapper/trapper/external_media
      - ./trapper/trapper-project/trapper/media:/opt/trapper/trapper/media
    command: run
  nginx:
    build: ./nginx
    container_name: nginx
    command: make run
    ports:
      - "80:80"
      - "443:443"
    links:
      - trapper
  ftpd:
    image: stilliard/pure-ftpd:hardened
    container_name: trapper_ftpd
    depends_on:
      - trapper
    environment:
      - PUBLICHOST=127.0.0.1
      - FTP_MAX_CLIENTS=200
      - FTP_MAX_CONNECTIONS=200
      - FTP_PASSIVE_PORTS=30000:30399
    ports:
      - "21:21"
      - "30000-30399:30000-30399"
    volumes:
      - ./trapper/trapper-project/trapper/external_media:/opt/trapper/trapper/external_media
      - ./ftp/psswd:/etc/pure-ftpd/passwd
      - ./ftp/ssl/private:/etc/ssl/private/
    command: >
      bash -c "chown ftpuser:ftpgroup -R /opt/trapper/trapper/external_media
      && /run.sh -l puredb:/etc/pure-ftpd/pureftpd.pdb -E -j -R -P ADD_HERE_YOUR_IP_SERVER_OR_SERVER_NAME"

volumes:
  pgdata:
